package com.kkhenissi.fdc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Item.
 */
@Entity
@Table(name = "item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Item implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "initial_price")
    private Double initialPrice;

    @Column(name = "current_price")
    private Double currentPrice;

    @Column(name = "sold_date")
    private Instant soldDate;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "status_item")
    private Boolean statusItem;

    @ManyToOne
    @JsonIgnoreProperties(value = "items", allowSetters = true)
    private SubCategory subCategory;

    @ManyToOne
    @JsonIgnoreProperties(value = "items", allowSetters = true)
    private Brand brand;

    @ManyToMany(mappedBy = "items")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<FdcUser> fdcUsers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Item name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Item description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getInitialPrice() {
        return initialPrice;
    }

    public Item initialPrice(Double initialPrice) {
        this.initialPrice = initialPrice;
        return this;
    }

    public void setInitialPrice(Double initialPrice) {
        this.initialPrice = initialPrice;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public Item currentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
        return this;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Instant getSoldDate() {
        return soldDate;
    }

    public Item soldDate(Instant soldDate) {
        this.soldDate = soldDate;
        return this;
    }

    public void setSoldDate(Instant soldDate) {
        this.soldDate = soldDate;
    }

    public byte[] getImage() {
        return image;
    }

    public Item image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Item imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Boolean isStatusItem() {
        return statusItem;
    }

    public Item statusItem(Boolean statusItem) {
        this.statusItem = statusItem;
        return this;
    }

    public void setStatusItem(Boolean statusItem) {
        this.statusItem = statusItem;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public Item subCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
        return this;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Brand getBrand() {
        return brand;
    }

    public Item brand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Set<FdcUser> getFdcUsers() {
        return fdcUsers;
    }

    public Item fdcUsers(Set<FdcUser> fdcUsers) {
        this.fdcUsers = fdcUsers;
        return this;
    }

    public Item addFdcUser(FdcUser fdcUser) {
        this.fdcUsers.add(fdcUser);
        fdcUser.getItems().add(this);
        return this;
    }

    public Item removeFdcUser(FdcUser fdcUser) {
        this.fdcUsers.remove(fdcUser);
        fdcUser.getItems().remove(this);
        return this;
    }

    public void setFdcUsers(Set<FdcUser> fdcUsers) {
        this.fdcUsers = fdcUsers;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Item)) {
            return false;
        }
        return id != null && id.equals(((Item) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Item{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", initialPrice=" + getInitialPrice() +
            ", currentPrice=" + getCurrentPrice() +
            ", soldDate='" + getSoldDate() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", statusItem='" + isStatusItem() + "'" +
            "}";
    }
}
