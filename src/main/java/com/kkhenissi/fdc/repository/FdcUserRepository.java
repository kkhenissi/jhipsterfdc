package com.kkhenissi.fdc.repository;

import com.kkhenissi.fdc.domain.FdcUser;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FdcUser entity.
 */
@Repository
public interface FdcUserRepository extends JpaRepository<FdcUser, Long> {
    @Query(
        value = "select distinct fdcUser from FdcUser fdcUser left join fetch fdcUser.items",
        countQuery = "select count(distinct fdcUser) from FdcUser fdcUser"
    )
    Page<FdcUser> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct fdcUser from FdcUser fdcUser left join fetch fdcUser.items")
    List<FdcUser> findAllWithEagerRelationships();

    @Query("select fdcUser from FdcUser fdcUser left join fetch fdcUser.items where fdcUser.id =:id")
    Optional<FdcUser> findOneWithEagerRelationships(@Param("id") Long id);
}
