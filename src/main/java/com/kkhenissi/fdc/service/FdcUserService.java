package com.kkhenissi.fdc.service;

import com.kkhenissi.fdc.domain.FdcUser;
import com.kkhenissi.fdc.repository.FdcUserRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FdcUser}.
 */
@Service
@Transactional
public class FdcUserService {
    private final Logger log = LoggerFactory.getLogger(FdcUserService.class);

    private final FdcUserRepository fdcUserRepository;

    public FdcUserService(FdcUserRepository fdcUserRepository) {
        this.fdcUserRepository = fdcUserRepository;
    }

    /**
     * Save a fdcUser.
     *
     * @param fdcUser the entity to save.
     * @return the persisted entity.
     */
    public FdcUser save(FdcUser fdcUser) {
        log.debug("Request to save FdcUser : {}", fdcUser);
        return fdcUserRepository.save(fdcUser);
    }

    /**
     * Get all the fdcUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FdcUser> findAll(Pageable pageable) {
        log.debug("Request to get all FdcUsers");
        return fdcUserRepository.findAll(pageable);
    }

    /**
     * Get all the fdcUsers with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<FdcUser> findAllWithEagerRelationships(Pageable pageable) {
        return fdcUserRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one fdcUser by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FdcUser> findOne(Long id) {
        log.debug("Request to get FdcUser : {}", id);
        return fdcUserRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the fdcUser by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FdcUser : {}", id);
        fdcUserRepository.deleteById(id);
    }
}
