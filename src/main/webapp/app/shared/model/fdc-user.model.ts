import { Moment } from 'moment';
import { IItem } from 'app/shared/model/item.model';
import { IDepartment } from 'app/shared/model/department.model';

export interface IFdcUser {
  id?: number;
  kindOfUser?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  hireDate?: Moment;
  items?: IItem[];
  department?: IDepartment;
}

export class FdcUser implements IFdcUser {
  constructor(
    public id?: number,
    public kindOfUser?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phoneNumber?: string,
    public hireDate?: Moment,
    public items?: IItem[],
    public department?: IDepartment
  ) {}
}
