import { Moment } from 'moment';
import { IItem } from 'app/shared/model/item.model';

export interface IBrand {
  id?: number;
  name?: string;
  startDate?: Moment;
  items?: IItem[];
}

export class Brand implements IBrand {
  constructor(public id?: number, public name?: string, public startDate?: Moment, public items?: IItem[]) {}
}
