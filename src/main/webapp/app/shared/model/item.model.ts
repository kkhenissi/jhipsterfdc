import { Moment } from 'moment';
import { ISubCategory } from 'app/shared/model/sub-category.model';
import { IBrand } from 'app/shared/model/brand.model';
import { IFdcUser } from 'app/shared/model/fdc-user.model';

export interface IItem {
  id?: number;
  name?: string;
  description?: string;
  initialPrice?: number;
  currentPrice?: number;
  soldDate?: Moment;
  imageContentType?: string;
  image?: any;
  statusItem?: boolean;
  subCategory?: ISubCategory;
  brand?: IBrand;
  fdcUsers?: IFdcUser[];
}

export class Item implements IItem {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public initialPrice?: number,
    public currentPrice?: number,
    public soldDate?: Moment,
    public imageContentType?: string,
    public image?: any,
    public statusItem?: boolean,
    public subCategory?: ISubCategory,
    public brand?: IBrand,
    public fdcUsers?: IFdcUser[]
  ) {
    this.statusItem = this.statusItem || false;
  }
}
