import { ICategory } from 'app/shared/model/category.model';

export interface ISubCategory {
  id?: number;
  name?: string;
  category?: ICategory;
}

export class SubCategory implements ISubCategory {
  constructor(public id?: number, public name?: string, public category?: ICategory) {}
}
